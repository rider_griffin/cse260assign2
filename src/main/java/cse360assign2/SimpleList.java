/*
 * Author: Rider Griffin
 * ID: 413
 * Assignment 2
 * Description: This file creates a class called SimpleList that creates an array
 * 				of 10 integers. The methods in this file are similar to assignment1,
 * 				but "add" and "remove" methods now increase/decrease the size of the list,
 * 				and new methods are added such as "append", "first", "last", and "size".
 * 
 * Repository Link: https://bitbucket.org/rider_griffin/cse260assign2/src/master/
 */ 

package cse360assign2;
/*
 * The class SimpleList contains private variables such as the array "list" and 
 * the integer "count", which are used to hold elements and keep track of how 
 * many numbers are in the array. The class also contains the methods listed above
 * which allow for array manipulation
 */
public class SimpleList 
{
	
	/*
	 * Class variables that declare an empty array of type integer and a count variable of
	 * type integer
	 */
	private int[] list;
	private int count;
	
	/*
	 * Constructor for the SimpleList class, which creates the array that holds
	 * 10 elements and sets the variable count to 0
	 */
	public SimpleList()        
	{
		list = new int[10];
		count = 0;
	}
	
	/*
	 * The add method takes a parameter of type integer and will always add the
	 * passed number to index 0, right-shifting previous numbers. If the number of 
	 * elements exceeds the size of the array, the array size is increased by 50% and the 
	 * input parameter is added to the front of the list.
	 */
	public void add(int number) 
	{
		if(count < list.length)
		{
			for(int index = count - 1; index >= 0; index--) 	
			{
				//shift all elements to the right by one
				list[index + 1] = list[index];		
			}
			
			//place the input number at index 0 and increment the count
			list[0] = number;
			count++;
		}
		
		//If the count is greater than 10, increase the size of the array by 50% and add the parameter
		else
		{ 
			//calculate new size of new array, and declare a bigger array with new size
			int newSize = (count / 2) + count;
			int[] biggerList = new int[newSize];
			
			//copy elements from original list into the bigger list
			for(int index = 0; index < count; index++)
			{
				biggerList[index] = list[index];
			}
			
			//store values from bigger list back into original list
			list = biggerList;
			
			//insert parameter at beginning and increase count
			for(int index = count - 1; index >=0; index--)
			{
				list[index + 1] = list[index];
			}
			list[0] = number;
			count++;
		}
	}
	
	/*
	 * The method search takes a parameter of type integer, which will be 
	 * the desired number we want to search for in the array. This method
	 * traverses the array and returns the index of the number if it has found
	 * a match, and will return -1 if the number does not exist in the list
	 */
	public int search(int number)
	{
		int searchedIndex = -1;
		for(int index = 0; index < count; index++)
		{
			//check each element if it is equal to the input number
			if(list[index] == number)
			{
				//change index if number is found
				searchedIndex = index;
			}
		}
		
		//return the integer that is the index of the searched number
		return searchedIndex;
	}
	
	/*
	 * The method remove takes an integer as a parameter and removes that integer
	 * from the list. This method uses the search function to find the index of 
	 * the input integer and removes it. If the search returns a -1, the list
	 * remains the same because the integer does not exist in the array. If the array 
	 * consists of more than 25% empty space, then the list's size is decreased by 25%
	 * of its size
	 */
	public void remove(int number)
	{
		//Call the search function with the input integer and store the index
		int foundIndex = search(number);
		
		if(foundIndex == -1)
		{
			//do nothing
		}
		
		//If the index does not equal -1, left-shift all the elements by one 
		//starting at the foundIndex
		else
		{
			for(int index = foundIndex; index < count - 1; index++)
			{
				list[index] = list[index + 1];
			}

			//Decrease the count after removal
			count--;
		}
		
		//find the empty spaces of the list and divide length by 4 to get quarter size
		int emptySlots = list.length - count;
		int quarterSize = list.length/4;
		
		//check if list has more than 1 element and if the emptySlots exceeds the quarterSize
		if(list.length - quarterSize >= 1 && emptySlots >= quarterSize) 
		{
			//declare new array with smaller size
			int[] smallerList = new int[list.length - quarterSize];
			
			//copy all elements into smaller list then back into the original list
			for(int index = 0; index < smallerList.length; index++) 
			{
				smallerList[index] = list[index];
			}
			list = smallerList;
		}

	}
	
	/*
	 * The method append takes an integer as a parameter and appends it to the 
	 * end of the list. If the list is full, the size of the list is increased
	 * by 50%, the number is still appended, and the count increases. 
	 */
	public void append(int number)
	{
		if(count == list.length) 
		{
			int newSize = (count / 2) + count;
			int[] biggerList = new int[newSize];
			
			//copy elements from original list into the bigger list
			for(int index = 0; index < count; index++)
			{
				biggerList[index] = list[index];
			}
			
			//store values from bigger list back into original list
			list = biggerList;
		}
		
		list[count] = number;
		count++;
	}
	
	/*
	 * The method first() does not take any parameters and returns the
	 * first element of the list of type integer. If the list is empty, a -1 is returned
	 */
	public int first()
	{
		int firstInt = -1;
		if (count > 0)
		{
			firstInt = list[0];
		}
		return firstInt;
	}
	
	/*
	 * The method last() does not take any parameters and returns the last
	 * element of the list that is type integer. If the list is empty, a -1 is returned
	 */
	public int last()
	{
		int lastInt = -1;
		if(count > 0)
		{
			lastInt = list[count - 1];
		}
		return lastInt;
	}
	
	/*
	 * The method size() provides the current number of possible locations in the list.
	 * It does not take any parameters and returns the total possible
	 * locations in the list. 
	 */
	public int size()
	{
		return list.length;
	}
	
	/*
	 * The method count does not take a parameter and returns the 
	 * number of elements within the list using the count variable
	 */
	public int count()
	{
		return count;
	}
	
	/*
	 * The method toString does not take a parameter, and returns the 
	 * contents of the list in the form of a string. The method traverses
	 * the list and copies each element to the printArray string with spaces
	 * in between each element except the last element in the list
	 */
	public String toString()
	{
		String printArray = "";
		for(int index = 0; index < count; index++)
		{
			//Copy each element in the array into the string
			printArray += list[index];
			
			//Place a space between each element unless it is the tail element
			if(index != count - 1)
			{
				printArray += " ";
			}
		}
		
		//return the list in the form of a string
		return printArray;
	}
}
